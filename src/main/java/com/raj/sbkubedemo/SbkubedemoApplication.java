package com.raj.sbkubedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbkubedemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbkubedemoApplication.class, args);
	}

}
