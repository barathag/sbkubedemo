package com.raj.sbkubedemo.controller;

import com.raj.sbkubedemo.model.Employee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    @GetMapping
    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();

        // Add 5 dummy employees (you can replace this with actual data from your database)
        employees.add(new Employee(1L, "John", "Doe", "john.doe@example.com"));
        employees.add(new Employee(2L, "Jane", "Doe", "jane.doe@example.com"));
        employees.add(new Employee(3L, "Alice", "Smith", "alice.smith@example.com"));
        employees.add(new Employee(4L, "Bob", "Johnson", "bob.johnson@example.com"));
        employees.add(new Employee(5L, "Eva", "Williams", "eva.williams@example.com"));

        return employees;
    }
}
